//
//  ViewController.swift
//  Yelp App Test Project
//
//  Created by Alina Chernenko on 6/12/17.
//  Copyright © 2017 dimalina. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }

}

