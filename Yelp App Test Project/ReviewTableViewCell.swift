//
//  ReviewTableViewCell.swift
//  Yelp App Test Project
//
//  Created by Alina Chernenko on 6/13/17.
//  Copyright © 2017 dimalina. All rights reserved.
//

import UIKit

class ReviewTableViewCell: UITableViewCell {

    @IBOutlet weak var reviewTextLabel: UILabel!
    @IBOutlet weak var ratingLabel: UILabel!{
        didSet{
            ratingLabel.clipsToBounds = true
            ratingLabel.layer.cornerRadius = ratingLabel.frame.width/2
            ratingLabel.backgroundColor = UIColor.orange
            ratingLabel.font = UIFont.boldSystemFont(ofSize: 12)
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
