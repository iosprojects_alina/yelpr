//
//  LocationDetailViewController.swift
//  Yelp App Test Project
//
//  Created by Alina Chernenko on 6/13/17.
//  Copyright © 2017 dimalina. All rights reserved.
//

import UIKit
import Cosmos
import YelpAPI

class LocationDetailViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!{
        didSet{
            tableView.delegate = self
            tableView.dataSource = self
        }
    }
    @IBOutlet weak var locationPhone: UILabel!
    @IBOutlet weak var locationName: UILabel!
    @IBOutlet weak var locationImage: UIImageView!
    @IBOutlet weak var ratingView: CosmosView!
    @IBOutlet weak var categoriesLabel: UILabel!
    var location: Location?
    var client: YelpAPIClient?
    var review: YLPBusinessReviews?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = location?.title
        locationPhone.text = location?.business?.phone
        locationName.text = location?.title
        locationImage.sd_setImage(with: location?.imageURL)
        locationImage.contentMode = .scaleAspectFit
        
        
        if let locB = location?.business {
            for category in locB.categories {
               let index = locB.categories.index(of: category)
                if index != (locB.categories.count - 1) {
                    let text = (categoriesLabel.text ?? "") + category.name
                    categoriesLabel.text = text + ", "
                } else {
                     categoriesLabel.text = category.name
                }
            }
        }
        
        ratingView.rating = location?.rating ?? 0
        ratingView.text = String(location?.business?.reviewCount ?? 0)
        self.loadReviews(id: location?.business?.identifier ?? "")
    }
    
    func loadReviews(id:String){
        self.client?.yelpClient?.reviewsForBusiness(withId: id, completionHandler: { (reviews, error) in
            DispatchQueue.main.async {
                self.review = reviews
                self.tableView.reloadData()
            }
        })
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.review?.reviews.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reviewCell") as? ReviewTableViewCell
        cell?.reviewTextLabel.text = self.review?.reviews[indexPath.row].excerpt
        cell?.ratingLabel.text = String(self.review?.reviews[indexPath.row].rating ?? 0)
        
        return cell!
    }

}
