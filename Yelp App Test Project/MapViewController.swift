//
//  MapViewController.swift
//  Yelp App Test Project
//
//  Created by Alina Chernenko on 6/12/17.
//  Copyright © 2017 dimalina. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation
import YelpAPI
import SDWebImage

class MapViewController: UIViewController, CLLocationManagerDelegate, MKMapViewDelegate {

    @IBOutlet weak var map: MKMapView!{
        didSet{
            map.delegate = self
        }
    }
    let manager = CLLocationManager()
    var myLocation: CLLocationCoordinate2D?
    var arrayOfBusinesses: [YLPBusiness]? {
        didSet{
            self.loadDataOnMap()
        }
    }
    var client: YelpAPIClient?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Map View"
        manager.delegate = self
        manager.desiredAccuracy = kCLLocationAccuracyBest
        manager.requestWhenInUseAuthorization()
        self.map.showsUserLocation = true
        manager.startUpdatingLocation()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = false
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location = locations[0]
        let span = MKCoordinateSpanMake(0.01, 0.01)
        myLocation = CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
        let region : MKCoordinateRegion = MKCoordinateRegionMake(myLocation!, span)
        map.setRegion(region, animated: true)
        
        if arrayOfBusinesses == nil {
            let yelpCoordinate = YLPCoordinate(latitude: myLocation!.latitude, longitude: myLocation!.longitude)
            self.client = YelpAPIClient(location: yelpCoordinate) { (businesses) in
                self.arrayOfBusinesses = businesses
                manager.stopUpdatingLocation()
            }
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedWhenInUse {
            manager.requestLocation()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
         print(error.localizedDescription)
    }

    
    func loadDataOnMap(){
        if self.arrayOfBusinesses != nil {
            for business in self.arrayOfBusinesses! {
                let location = Location(name: business.name, location: business.location, imageURL: business.imageURL, rating: business.rating, business: business)
                self.map.addAnnotation(location)
            }
        }
    }
    
    func mapView(_ mapView: MKMapView, regionDidChangeAnimated animated: Bool) {
        let centerLocation = mapView.centerCoordinate
        let yelpCoordinate = YLPCoordinate(latitude: centerLocation.latitude, longitude: centerLocation.longitude)
        client?.searchPlaces(with: yelpCoordinate, completionHandler: { (businesses) in
            self.arrayOfBusinesses = businesses
        })
    }
    
    func mapView(_ mapView: MKMapView, didAdd views: [MKAnnotationView]) {
       
            for annotationView in views {
                let location = annotationView.annotation?.coordinate
                if MKMapRectContainsPoint(mapView.visibleMapRect, MKMapPointForCoordinate(location!)){
                    let annotationView = mapView.view(for: annotationView.annotation!)
                        animateTheAnnotation(annotationView: annotationView!)
                }
            }
            
        
    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        
        guard !annotation.isKind(of: MKUserLocation.self) else {
            return nil
        }
        
        let reuseIdentifier = "pin"
        
        var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: reuseIdentifier)
        if annotationView == nil {
            annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: reuseIdentifier)
        } else {
            annotationView?.annotation = annotation
        }
        let customAnnotationView = CustomAnnotationView(annotation: annotation, reuseIdentifier: reuseIdentifier)
        
        customAnnotationView.image = UIImage(named: "customPin")
        annotationView = customAnnotationView
        return annotationView
    }
    
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        
        if view.annotation is MKUserLocation {
            return
        }
        
        if view != currentView {
            currentView?.removeFromSuperview()
        }
        
        let objectAnnotation = view.annotation as! Location
        
        let iconLabel = UILabel(frame: CGRect(x:2, y:2, width:29, height:29))
        iconLabel.clipsToBounds = true
        iconLabel.layer.cornerRadius = iconLabel.frame.width/2
        iconLabel.backgroundColor = UIColor.orange
        iconLabel.text = String(objectAnnotation.rating!)
        iconLabel.font = UIFont.boldSystemFont(ofSize: 17)
        iconLabel.textAlignment = .center
        
        let annotationLabel = UILabel()
        annotationLabel.text = objectAnnotation.title
        let labelSize = self.estimatedLabelSize(label: annotationLabel)
        annotationLabel.frame = CGRect(x:36, y:5, width:ceil(labelSize.width)+10, height:25)
        annotationLabel.font = UIFont.boldSystemFont(ofSize: 17)
        
        let annotationAddressLabel = UILabel()
        annotationAddressLabel.text = objectAnnotation.address
        let addressLabelSize = self.estimatedLabelSize(label: annotationAddressLabel)
        annotationAddressLabel.frame = CGRect(x:5, y:35, width:ceil(addressLabelSize.width)+10, height:25)
        
        let labelWidth = (annotationAddressLabel.frame.width > annotationLabel.frame.width) ? annotationAddressLabel.frame.width: (annotationLabel.frame.width + 30)
        
        let arrowImage = UIImageView(frame: CGRect(x:5+labelWidth, y:27, width:15, height:15))
        arrowImage.image = UIImage(named: "icon_row_arrow")
        
        let annotationViewWidth = labelWidth + arrowImage.frame.width + 14
        
        let calloutView = CustomCalloutView(frame: CGRect(x:0, y:-70, width:annotationViewWidth, height:70), rightImage: iconLabel, calloutLabel: annotationLabel, calloutArrow: arrowImage, address: annotationAddressLabel)
        calloutView.backgroundColor = UIColor.white
        calloutView.layer.cornerRadius = 10
        calloutView.layer.borderColor = UIColor.red.cgColor
        calloutView.layer.borderWidth = 1
        
        self.currentView = view as? CustomAnnotationView
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(openLocationDetail))
        tapGesture.numberOfTapsRequired = 1
        calloutView.addGestureRecognizer(tapGesture)
        
        calloutView.center = CGPoint(x: view.bounds.size.width / 2, y: -calloutView.bounds.size.height*0.52)
        view.addSubview(calloutView)
        mapView.setCenter((view.annotation?.coordinate)!, animated: true)
    }
    
    var currentView: CustomAnnotationView?
    func openLocationDetail(){
        performSegue(withIdentifier: "showLocationDetail", sender: self.currentView)
    }
    
    func animateTheAnnotation(annotationView:MKAnnotationView){
        UIView.animate(withDuration: 0.5, delay: 0.3, options: UIViewAnimationOptions.curveEaseIn, animations:{() in
            // Animate squash
        }, completion:{(Bool) in
            UIView.animate(withDuration: 0.05, delay: 0.0, options: UIViewAnimationOptions.curveEaseInOut, animations:{() in
                annotationView.transform = CGAffineTransform(scaleX: 1.2, y: 1.2)
                
            }, completion: {(Bool) in
                UIView.animate(withDuration: 0.3, delay: 0.0, options: UIViewAnimationOptions.curveEaseInOut, animations:{() in
                    annotationView.transform = CGAffineTransform.identity
                }, completion: nil)
            })
            
        })
        
    }
    
    /**
     Function to estimate the label size
     
     - Parameters:
     - label:UILabel
     
     - returns: CGSize
     */
    func estimatedLabelSize(label: UILabel) -> CGSize {
        guard let text = label.text else { return .zero }
        return NSString(string: text).boundingRect(with: CGSize(width:CGFloat.greatestFiniteMagnitude, height:CGFloat.greatestFiniteMagnitude), options: .usesLineFragmentOrigin, attributes: [NSFontAttributeName: label.font], context: nil).size
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let check = currentView?.isInside, !check {
            currentView?.removeFromSuperview()
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showLocationDetail" {
            let sceneVC = segue.destination as? LocationDetailViewController
            let annotationView = sender as! MKAnnotationView
            let annotation = annotationView.annotation as? Location
            sceneVC?.location = annotation
            sceneVC?.client = self.client
            
        }
    }
    

}


