//
//  YelpAPIClient.swift
//  Yelp App Test Project
//
//  Created by Alina Chernenko on 6/12/17.
//  Copyright © 2017 dimalina. All rights reserved.
//

import UIKit
import YelpAPI


struct YelpAPIConsole {
    var clientID = "lbKOo30C0CexYuH5ldDOfQ"
    var clientSecret = "QFB4u6jRok7NogIoTuZDzgY0vBoqUeSyXIuwGofpZzNTCsIXbGCXBIc99NiX23Yt"
}

class YelpAPIClient: NSObject {
    
    var yelpConsole:YelpAPIConsole?
    var yelpClient : YLPClient?
    
    //static var sharedInstance = YelpAPIClient()
    
    init(location: YLPCoordinate, completionHandler: @escaping ([YLPBusiness]?) -> Void){
        super.init()
        self.yelpConsole = YelpAPIConsole()
        YLPClient.authorize(withAppId: self.yelpConsole!.clientID, secret: self.yelpConsole!.clientSecret) { (client, error) in
            self.yelpClient = client
            self.searchPlaces(with: location, completionHandler: completionHandler)
            }
        }
    
    func searchPlaces(with coordinate: YLPCoordinate, completionHandler: @escaping ([YLPBusiness]?) -> Void){
        yelpClient?.search(with: coordinate, term: "restaurant", limit: 40, offset: 0, sort: .distance, completionHandler: { (search, error) in
            if error == nil {
                completionHandler(search?.businesses)
            }
        })
        
    }
    
}
