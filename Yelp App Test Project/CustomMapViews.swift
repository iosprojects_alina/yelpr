//
//  CustomMapViews.swift
//  Yelp App Test Project
//
//  Created by Alina Chernenko on 6/13/17.
//  Copyright © 2017 dimalina. All rights reserved.
//

import UIKit
import MapKit
import YelpAPI

class Location: NSObject, MKAnnotation {
    let title: String?
    let coordinate: CLLocationCoordinate2D
    let imageURL: URL?
    let address: String?
    let rating: Double?
    let business: YLPBusiness?
    
    init(name: String, location: YLPLocation,imageURL:URL?, rating:Double, business:YLPBusiness) {
        self.title = name
        self.coordinate = CLLocationCoordinate2D(latitude: location.coordinate?.latitude ?? 0, longitude: location.coordinate?.longitude ?? 0)
        self.imageURL = imageURL
        self.address = location.address.first
        self.rating = rating
        self.business = business
        super.init()
    }
}

class CustomAnnotationView: MKAnnotationView {
    var isInside = false
    
    override func hitTest(_ point: CGPoint, with event: UIEvent?) -> UIView? {
        let hitView = super.hitTest(point, with: event)
        if (hitView != nil)
        {
            self.superview?.bringSubview(toFront: self)
        }
        return hitView
    }
    override func point(inside point: CGPoint, with event: UIEvent?) -> Bool {
        let rect = self.bounds
        var isInside: Bool = rect.contains(point)
        if(!isInside)
        {
            for view in self.subviews
            {
                isInside = view.frame.contains(point)
                if isInside
                {
                    self.isInside = isInside
                    break
                }
            }
        }
        return isInside
    }
}

class CustomCalloutView: UIView {
    let rightImage:UILabel?
    let calloutLabel:UILabel?
    let calloutArrow:UIImageView?
    let addressLabel: UILabel?
    
    init(frame: CGRect, rightImage:UILabel, calloutLabel:UILabel, calloutArrow:UIImageView, address: UILabel) {
        self.rightImage = rightImage
        self.calloutLabel = calloutLabel
        self.calloutArrow = calloutArrow
        self.addressLabel = address
        super.init(frame: frame)
        self.addSubview(rightImage)
        self.addSubview(calloutLabel)
        self.addSubview(calloutArrow)
        self.addSubview(address)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

